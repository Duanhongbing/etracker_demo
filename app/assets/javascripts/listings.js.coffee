# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/
$(document).on("click", "a[href='javascript:void(0);']", (r) ->
  window.open($(@).data('url'), "newindow", "height=750, width=1000, top=0, left=0, toolbar=no, menubar=no");
)
