module ApplicationHelper
  def is_active?(name)
    controller_name == name ? 'active': ''
  end
end
