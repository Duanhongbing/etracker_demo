class Listing < ActiveRecord::Base
  validates :name, :asin, presence: true
  validates :channel_id, presence: true

  belongs_to :channel
  has_and_belongs_to_many :tags
  has_many :listing_histories

  def fetch_listing
    str = request_json
    parse_json_to_history(str)
  end

  def request_json
    http = Curl::Easy.new(base_url(self.channel.sort_name, asin)) do |c|
      c.encoding = 'gzip'
      c.headers['User-Agent'] = 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_7_5) AppleWebKit/537.11 (KHTML, like Gecko) Chrome/23.0.1271.101 Safari/537.11'
      c.headers['Accept-Charset'] = 'utf-8'
      c.timeout = 20
    end
    http.perform
    response = http.body_str
    if is_normal_result?(response)
      return response
    else
      request_json
    end
  end

  def parse_json_to_history(json_str)
    listing_history = self.listing_histories.build
    json = JSON.parse(json_str)
    listing_history.title = json['title']
    listing_history.price = json['price'].to_f
    listing_history.seller_name = json['by_who']
    listing_history.main_img_url = json['main_image']
    listing_history.prod_desc = json['product_desc']
    listing_history.rating_score = json['review_rating']
    listing_history
  end

  def is_normal_result?(json)
    if json.nil? || json.include?('error')
      false
    else
      true
    end
  end

  def base_url(market, asin)
    "http://crawl.easya.cc/listing/#{market}/#{asin}.json"
  end
end
