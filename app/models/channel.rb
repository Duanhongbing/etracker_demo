class Channel < ActiveRecord::Base
  before_update :set_edit_times
  validates :name, presence: true, uniqueness: true
  has_many :listings

  def sort_name
    case self.name.downcase
      when 'amazon.com', 'amazon_us', 'us'
        'us'
      when 'amazon.co.uk', 'amazon_uk', 'uk'
        'uk'
      when 'amazon.fr', 'amazon_fr', 'fr'
        'fr'
      when 'amazon.es', 'amazon_es', 'es'
        'es'
      when 'amazon.de', 'amazon_de', 'de'
        'de'
      when 'amazon.it', 'amazon_it', 'it'
        'it'
      when 'amazon.co.jp', 'amazon_jp', 'jp'
        'jp'
      else
        raise RuntimeError('Error, invalid channel name!')
    end
  end

  private
  def set_edit_times
    self.edit_times = self.edit_times + 1
  end
end
