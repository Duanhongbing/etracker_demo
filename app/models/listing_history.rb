class ListingHistory < ActiveRecord::Base
  belongs_to :listing
  validates :listing_id, presence: true
  paginates_per 3
end
