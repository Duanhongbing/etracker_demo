class User < ActiveRecord::Base
  validates :username, presence: true, uniqueness: true, length: {maximum: 25}
  validates :password, :password_confirmation, presence: true, length: {maximum: 25}
  has_secure_password

  before_save :downcase_username
  before_create :build_remember_token

  private
  def downcase_username
    self.username = self.username.downcase
  end

  # 这里创建一个随机字符
  def User.new_remember_token
    SecureRandom.urlsafe_base64
  end

  # 将随机字符进行加密
  def User.hash(token)
    Digest::SHA1.hexdigest(token.to_s)
  end

  private

    def build_remember_token
      self.remember_token = User.hash(User.new_remember_token)
    end
end
