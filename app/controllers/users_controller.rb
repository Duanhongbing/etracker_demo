class UsersController < ApplicationController
  skip_before_action :signed_in_user, only:[:new, :create]
  before_action :is_current_user, only: [:show]

  def new
    @user = User.new
  end

  def create
    @user = User.new(build_user_params)
    if @user.save
      flash[:success] = '注册成功!'
      sign_in(@user)
      redirect_to @user
    else
      render 'new'
    end
  end

  def show
    @user = User.find(params[:id])
  end


  private

  def build_user_params
    params.require(:user).permit(:username, :password, :password_confirmation)
  end

  def is_current_user
    @user = User.find_by(params[:id])
    redirect_to(root_url) unless current_user?(@user)
  end
end
