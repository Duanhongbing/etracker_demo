class ListingsController < ApplicationController
  def index
    @listings = Listing.all
  end

  def new
    @listing = build_listing
  end

  def create
    @listing = Listing.new(new_listing_params)
    if @listing.save
      flash[:success] = '添加 Listing 成功'
      redirect_to listings_url
    else
      render 'new'
    end
  end

  def show
    @listing = Listing.find(params[:id])
  end

  def edit
    @listing = Listing.find(params[:id])
  end

  def update
    @listing = Listing.find(params[:id])
    if @listing.update(update_listing_params)
      flash.now[:success] = '修改成功.'
      redirect_to @listing
    else
      render 'edit'
    end
  end

  def destroy
    @listing = Listing.find(params[:id])
    if @listing.destroy
      flash[:success] = '删除成功.'
    else
      flash[:danger] = '删除失败.'
    end
    redirect_to listings_url
  end

  def new_history
    @listing = Listing.find(params[:id])
    @listing_history = @listing.fetch_listing
    respond_to do |format|
      if @listing_history.save
        format.js
      end
    end
  end

  def historys
    listing = Listing.find(params[:id])
    @listing_histories = listing.listing_histories.order('created_at DESC').page(params[:page])
  end

  def prod_desc
    @listing_history = ListingHistory.find(params[:id])
  end

  private

  def build_listing
    if params[:channel_id]
      channel = Channel.find(params[:channel_id])
      channel.listings.build
    else
      Listing.new
    end
  end

  def new_listing_params
    params.require(:listing).permit(:name, :asin, :channel_id)
  end

  def update_listing_params
    params.require(:listing).permit(:name, :asin, :channel_id, :tag_ids => [])
  end
end
