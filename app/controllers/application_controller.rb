class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception
  before_action :signed_in_user, except: [:wellcome]

  include SessionsHelper

  def wellcome
  end


  def signed_in_user
    unless signed_in?
      store_url
      flash[:warning] = '请登录后再尝试'
      redirect_to signin_url
    end
  end
end
