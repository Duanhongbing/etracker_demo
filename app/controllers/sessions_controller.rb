class SessionsController < ApplicationController
  skip_before_action :signed_in_user, only:[:new, :create]

  def new
  end

  def create
    user = User.find_by(username: params[:session][:username].downcase)
    if user && user.authenticate(params[:session][:password])
      sign_in(user)
      redirect_or_back user
    else
      flash.now[:danger] = '用户名或密码错误, 请确认后再重试.'
      render 'new'
    end
  end

  def destroy
    sign_out
    redirect_to root_url
  end
end
