class ChannelsController < ApplicationController
  def index
    @channels = Channel.all
  end

  def new
    @channel = Channel.new
  end

  def create
    @channel = Channel.new(channel_params)
    if @channel.save
      flash[:success] = '创建成功.'
      redirect_to channels_url
    else
      render 'new'
    end
  end

  def show
    @channel = Channel.find(params[:id])
  end

  def destroy
    @channel = Channel.find(params[:id])
    if @channel.destroy
      flash[:success] = '删除成功.'
    else
      flash[:danger] = '删除失败.'
    end
    redirect_to channels_url
  end

  def edit
    @channel = Channel.find(params[:id])
    render 'new'
  end

  def update
    @channel = Channel.find(params[:id])
    if @channel.update(channel_params)
      flash.now[:success] = '修改成功.'
    else
      flash.now[:danger] = '修改失败.'
    end
    render 'new'
  end

  private
  def channel_params
    params.require(:channel).permit(:name)
  end
end
