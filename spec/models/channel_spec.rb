require 'spec_helper'

describe Channel do
  before do
    @channel = Channel.new(name: 'Example Channel')
  end

  subject { @channel }

  it { should respond_to(:name) }
  it { should be_valid }

  describe '当 name 为空的时候' do
    before { @channel.name='' }
    it { should_not be_valid  }
  end

  describe '当 name 已经存在时' do
    before do
      dup_channel = @channel.dup
      dup_channel.save
    end

    it { should_not be_valid }
  end
end