Rails.application.routes.draw do
  root 'application#wellcome'
  match '/signup', to: 'users#new', via: 'get'
  match '/signin', to: 'sessions#new', via: 'get'
  match '/signout', to: 'sessions#destroy', via: 'delete'
  match '/listing_historys/:id/prod_desc', to: 'listings#prod_desc', via: 'get', :as => 'prod_desc'
  resources :users
  resources :sessions, only: [:new, :create, :destroy]

  resources :channels do
    member do
      get 'listings/new', to: 'listings#new'
    end
  end

  resources :listings do
    member do
      post '/historys', to: 'listings#new_history'
      get '/historys', to: 'listings#historys'
    end
  end
end
