# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20140726071355) do

  create_table "channels", force: true do |t|
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "name",                   null: false
    t.integer  "edit_times", default: 0
  end

  add_index "channels", ["name"], name: "index_channels_on_name", using: :btree

  create_table "listing_histories", force: true do |t|
    t.text     "title"
    t.string   "price_str"
    t.float    "price"
    t.string   "currency"
    t.string   "seller_name"
    t.text     "main_img_url"
    t.text     "sale_rank_fragment"
    t.text     "prod_desc"
    t.float    "rating_score"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "listing_id"
  end

  create_table "listings", force: true do |t|
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "asin"
    t.string   "name"
    t.integer  "channel_id"
  end

  add_index "listings", ["channel_id"], name: "index_listings_on_channel_id", using: :btree

  create_table "listings_tags", id: false, force: true do |t|
    t.integer "listing_id"
    t.integer "tag_id"
  end

  create_table "tags", force: true do |t|
    t.string   "name"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "users", force: true do |t|
    t.string   "username"
    t.string   "password"
    t.string   "password_digest"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "remember_token"
  end

  add_index "users", ["remember_token"], name: "index_users_on_remember_token", using: :btree
  add_index "users", ["username"], name: "index_users_on_username", using: :btree

end
