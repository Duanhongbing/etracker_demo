class AddListingIdToListingHistories < ActiveRecord::Migration
  def change
    add_column :listing_histories, :listing_id, :integer
  end
end
