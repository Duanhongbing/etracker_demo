class AddNameToChannels < ActiveRecord::Migration
  def change
    add_column :channels, :name, :string, null: false
    add_index :channels, :name
  end
end
