class AddNameAsinToListings < ActiveRecord::Migration
  def change
    add_column :listings, :asin, :string
    add_column :listings, :name, :string
    add_column :listings, :channel_id, :integer
    add_index :listings, :channel_id, :null => false
  end
end
