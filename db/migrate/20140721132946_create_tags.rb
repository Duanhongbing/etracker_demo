class CreateTags < ActiveRecord::Migration
  def change
    create_table :tags do |t|
      t.string :name

      t.timestamps
    end

    create_table :listings_tags, id: false do |t|
      t.belongs_to :listing
      t.belongs_to :tag
    end
  end
end
